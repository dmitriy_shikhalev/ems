Engineering measurement system
==============================

Copyright 2016 Dmitriy Shikhalev <dmitriy.shikhalev@gmail.com>
This file is part of "ems".

    "ems" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "ems" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "ems".  If not, see <http://www.gnu.org/licenses/>.


Requirements:
  - apt-get install python3-dev
  - apt-get install libssl-dev
  - apt-get install libfreetype6-dev
  - apt-get install libxft-dev
  - apt-get install libxft2-dbg
  - apt-get install g++

- device daemon
- one deice - one serial port
- interface with plot and export-import functions