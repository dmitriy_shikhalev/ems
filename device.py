import serial
import protocols
import db
import time


class Device:
    """
    Device base class.
    """
    
    commands = []
    
    last_time = None
    period = None
    
    def __init__(self, session_db, name, config):
        super()
        
        self.name = name
        self.session_db = session_db
        self.config = config
        self.period = config['period']
        
        self.commands_db = []
        
        self.create_command_in_db()
    
    @property
    def name(self):
        return self.__name__
    
    def _get_serial(self):
        if self._serial:
            return self._serial
        # else:
        try:
            self._serial = serial.Serial(
                self.portname,
                baudrate=9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout = self.TIMEOUT,
            )
            return self._serial
        except serial.serialutil.SerialException:
            return None
    
    def close(self):
        try:
            self._serial.close()
        except:
            pass
        self._serial = None
    
    def create_command_in_db(self):
        self.device_db = db.Device(name=self.name, session=self.session_db)
        self.device_db.save()
        
        for command in self.commands:
            command_db = db.Command(name=command, device=self.device_db)
            command_db.save()
            self.commands_db.append(command_db)
    
    def lunch_command(self, command):
        ser = self._get_serial()
        if ser:
            result = getattr(self, 'get_'+command)()
            return result
    
    def time_to_get(self):
        if self.last_time is None:
            self.last_time = time.time()
        dt = time.time()
        if dt - self.last_time > self.period:
            self.last_time = dt
            return True
        return False
    
    def run(self):
        if self.time_to_get()
            for i, command in enumerate(self.commands):
                result = self.lunch_command(command)
                if result:
                    val = db.Value(
                        command=self.commands_db[i],
                        value=result
                    )
                    val.save()

class DeviceList:
    """
    List of devices.
    """
    def __init__(self, *args):
        self._device_list = list(args)
    
    def add(self, device):
        self.device_list.append(device)
    
    def run(self):
        for dev in self.device_list:
            dev.run()


class TRM200(Device):
    commands = ['1', '2']
    
    def get_1(self):
        command = '#PV'
    
    

DEVICES = {
    'ТРМ-200': TRM200,
}

if __name__ == '__main__':
    u = TRM200('COM1', 3)
    res = u.run()
