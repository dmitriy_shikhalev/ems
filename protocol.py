"""
Protocols:
    - Owen
    - Modbus (not realized)
    - clean
"""

from abc import ABCMeta, abstractmethod


class Protocol(metaclass=ABCMeta):
    @abstractmethod
    def encode(self):
        raise NotImplementedError
    
    @abstractmethod
    def decode(self):
        raise NotImplementedError


class Owen(Protocol):
    """
    Протокол Owen.
    Примеры:
        #HIHGROTVSPTM - запрос температуры (PV), тетьий канал
        #HIGKROTVKITJJJJJHSRO - реальная величина 105.6
        >>> round(f, 1)
        105.6
    """
    
    class CRCError(Exception): pass
    class UncorrectMessage(Exception): pass
    
    HALF_DICT = {
        0x00: b'G'[0],
        0x01: b'H'[0],
        0x02: b'I'[0],
        0x03: b'J'[0],
        0x04: b'K'[0],
        0x05: b'L'[0],
        0x06: b'M'[0],
        0x07: b'N'[0],
        0x08: b'O'[0],
        0x09: b'P'[0],
        0x0A: b'Q'[0],
        0x0B: b'R'[0],
        0x0C: b'S'[0],
        0x0D: b'T'[0],
        0x0E: b'U'[0],
        0x0F: b'V'[0],
    }
    
    REVERT_DICT = {
        b'G': 0x00,
        b'H': 0x01,
        b'I': 0x02,
        b'J': 0x03,
        b'K': 0x04,
        b'L': 0x05,
        b'M': 0x06,
        b'N': 0x07,
        b'O': 0x08,
        b'P': 0x09,
        b'Q': 0x0A,
        b'R': 0x0B,
        b'S': 0x0C,
        b'T': 0x0D,
        b'U': 0x0E,
        b'V': 0x0F,
    }
    
    CRC_BYTE = 0x44
    FIRST_BYTE = b'#'
    LAST_BYTE = b'\x0D'
    
    @classmethod
    def calculate_crc(cls, decoded_bytes):
        def hosh(byte, nbit, CRC):
            for i in range(0, nbit):
                if (byte ^ (CRC >> 8)) & 0x80:
                    CRC <<=1
                    CRC %= 65536
                    CRC ^=0x8f57
                else:
                    CRC <<=1
                    CRC %= 65536
                byte <<= 1
                byte %= 256
            return CRC

        def hosh_full(arr):
            crc = 0
            for a in arr:
                    crc = hosh(a, 8, crc)
            return (crc // 256, crc % 256)
        
        
        return hosh_full(decoded_bytes)
    
    @classmethod
    def encode(cls, decoded_bytes):
        decoded_bytes += cls.calculate_crc(decoded_bytes)
        
        encoded_bytes = bytearray()
        for byte in decoded_bytes:
            first, second = byte >> 4, byte % 2**4
            encoded_bytes += bytes([cls.HALF_DICT[first]])
            encoded_bytes += bytes([cls.HALF_DICT[second]])
        result = cls.FIRST_BYTE + bytes(encoded_bytes) + cls.LAST_BYTE
        
        return result
    
    @classmethod
    def strip(cls, encoded_bytes):
        if encoded_bytes[0] == cls.FIRST_BYTE[0]:
            encoded_bytes = encoded_bytes[1:]
        if encoded_bytes[-1] == cls.LAST_BYTE[0]:
            encoded_bytes = encoded_bytes[:-1]
        return bytes(encoded_bytes)
    
    @classmethod
    def decode(cls, encoded_bytes):
        encoded_bytes = cls.strip(encoded_bytes)
        
        decoded_bytes = bytearray()
        for i in range(0, len(encoded_bytes), 2):
            first, second = encoded_bytes[i:i+1], encoded_bytes[i+1:i+2]
            
            byte = (
                cls.REVERT_DICT[first] << 4
            ) + cls.REVERT_DICT[second]
            decoded_bytes.append(byte)
        
        if bytes(cls.calculate_crc(decoded_bytes[:-2])) != decoded_bytes[-2:]:
            raise cls.CRCError(encoded_bytes, bytes(cls.calculate_crc(decoded_bytes[:-2])), decoded_bytes[-2:])
        
        return bytes(decoded_bytes[:-2])



class Modbus(Protocol):
    pass



class Clean(Protocol):
    @classmethod
    def encode(cls, bytes):
        return bytes
    
    @classmethod
    def decode(cls, bytes):
        return bytes



if __name__ == '__main__':
    print(Owen.decode(b'#HIHGROTVSPTM\r'))
    print(Owen.decode(b'#HIGKROTVKITJJJJJHSRO\r'))
