"""
Define "work horse" in project.
This module contains realization
of all processes of measurement.
"""

import threading
import datetime
import time
from serial import Serial as RealSerial
from serial import PARITY_NONE, STOPBITS_ONE, EIGHTBITS

import db
import protocol

TIMEOUT = 1

SERIAL_PARAMETERS = dict(
    parity = PARITY_NONE,
    stopbits = STOPBITS_ONE,
    bytesize = EIGHTBITS,
    timeout = TIMEOUT
)


class Session(threading.Thread):
    """
    Main class.
    """
    
    def __init__(self, config):
        super().__init__()
        
        self.config = config
        self.init_db()
        
        self.init_serial_list()
        
        self._to_stop = False
    
    def init_serial_list(self):
        self.serial_list = []
        for ser_cfg in self.config:
            self.serial_list.append(
                Serial(ser_cfg, self.db),
                self.db,
            )
    
    def start(self):
        for serial in self.serial_list: serial.start() # start all
                                                       # serials
        super().start()
    
    def run(self):
        while not self._to_stop:
            time.sleep(SAVE_PERIOD)
            self.db.commit()
        
        # Wait for terminating of child threads
        for serial in self.serial_list:
            serial.join()
    
    def stop(self):
        for serial in self.serial_list:
            serial.stop()
    
    def init_db(self):
        num = 0
        name = datetime.date.today().strftime('%Y-%m-%d')
        db_name_set = set(db.Db._get_available_names())
        while True:
            dbname = name + ('-%03d'%num)
            if dbname not in db_name_set:
                self.db = db.Db(dbname)
                break
            num += 1


class Serial(threading.Thread):
    """
    Class for one serial port.
    """
    
    def __init__(self, ser_cfg, db):
        self.ser_cfg = ser_cfg
        self.db = db
        
        self._to_stop = False
        
        # Create real serial
        self._serial = RealSerial(self.ser_cfg.name, **SERIAL_PARAMETERS)
        
        self.init_dev_list()
    
    def init_dev_list(self):
        self.dev_list = []
        for dev_cfg in self.ser_cfg:
            self.dev_list.append(
                Device(dev_cfg)
            )
            
            # Init in db (device, command)
            dev_id = self.db.create_device(self.ser_cfg.name)
            for command in self.ser_cfg:
                id = self.db.create_command(dev_id, command.name)
                command.db_id = id
    
    @property
    def protocol(self):
        return self.ser_cfg.protocol
    
    def read(self):
        return self._serial.read()
    
    def write(self, bs):
        self._serial.write(bs)
    
    def run(self):
        while not self._to_stop:
            for device in self.device_list:
                device.loop()
    
    def stop(self):
        self._to_stop = True


if __name__ == '__main__':
    from config import Config
    conf = Config('test.cfg')
    sess = Session(conf)
