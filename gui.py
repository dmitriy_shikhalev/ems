"""
GUI of the project.
"""

from tkinter import *

root = Tk()
root.geometry('%dx%d+500+500' % (300, 300))

class Choice(Frame):
    """
    Must be:
        - Start new session.
        - Open old session.
        - Export one session.
        - Import one session.
        - Remove one session.
    """
    
    def __init__(self):
        super().__init__(root, bg='blue')
        
        self.new_session = Button(self, width=15, height=1, text='New session')
        self.new_session.grid(row=1, column=1)
        
        self.open_session = Button(self, width=15, height=1, text='Open session')
        self.open_session.grid(row=2, column=1)
        
        self.export_session = Button(self, width=15, height=1, text='Export session')
        self.export_session.grid(row=3, column=1)
        
        self.import_session = Button(self, width=15, height=1, text='Import session')
        self.import_session.grid(row=4, column=1)
        
        self.remove_session = Button(self, width=15, height=1, text='Remove session')
        self.remove_session.grid(row=5, column=1)

class Session(Frame):
    """
    Session window.
    """
    
    def __init__(self):
        super().__init__(root, bg='green', bd=5)
        self.config(width=1000, height=600)
        
        self.button_new = Button(self, width=10, height=10, text='Start')
        self.button_new.pack(expand=True, fill=X)
        self.button_load = Button(self, text='Stop', width=10, height=10)
        self.button_load.pack(expand=True, fill=X)

if __name__ == '__main__':
    print(TOP)
    # import tkinter
    # print(dir(tkinter))
    first = Choice()
    first.pack(expand=True, fill=BOTH)

    root.mainloop()
