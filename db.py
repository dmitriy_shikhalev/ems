"""
This module realize simple ORM.
    - session
    - device
    - command
    - value
"""

import os
from peewee import *
from datetime import datetime, date

DBNAME = 'db.sqlite3'

db = SqliteDatabase(DBNAME)

class Session(Model):
	name = CharField()
	date = DateField(default = date.today)
	
	class Meta:
		database = db

class Device(Model):
	session = ForeignKeyField(Session, related_name='devices')
	name = CharField()
	
	class Meta:
		database = db

class Command(Model):
    device = ForeignKeyField(Device, related_name='commands')
    name = CharField()
    
    class Meta:
        database = db

class Value(Model):
	command = ForeignKeyField(Command, related_name='values')
	dt = DateTimeField(default = datetime.now)
	value = DecimalField()
	
	class Meta:
		database = db


# Create db-file (for first time)
if not os.path.exists(DBNAME):
    db.create_tables([Session, Device, Command, Value])

if __name__ == '__main__': # just simle test
	s = Session(name='test')
	s.save()
	d = Device(name='TRM', session=s)
	d.save()
	c = Command(name='get1', device=d)
	c.save()
	v = Value(value=3.44, command=c)
	v.save()
	
	print(Value.select().count())
	for v in Value.select():
		print('v', v.dt, v.command.device_id, v.value)
