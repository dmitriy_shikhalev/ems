"""
This module load the config
from xml-file and returns object.
"""


from xmldict_translate import xml2dict
import sys


CONFIG_FILENAME = 'config.xml'


class Config:
    """
    This class is singletone for session configuration.
    """
    
    def __init__(self, filename):
        self.filename = filename
        self.dic = xml2dict(open(self.filename, 'rb').read())
    
    def __iter__(self):
        for subdic in self.dic['config'][0]['device']:
            yield subdic['_attrs_']
    
    def __str__(self):
        return 'Config: {} devices'.format(len(list(self)))

# Use this object:
config_instance = Config(CONFIG_FILENAME)


if __name__ == '__main__':
    print(config_instance)
    for dev in config_instance:
        print(dev)
        
